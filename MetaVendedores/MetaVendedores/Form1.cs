﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetaVendedores
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static class Html
        {
            public static string HtmlString;
        }
        public static class Body
        {
            public static string Bodyhtml;
        }
        public static class TableMes
        {
            public static string TableMesHtml;
        }
        public static class TableAcum
        {
            public static string TableAcumHtml;
        }
        public static class Facturado
        {
            public static decimal TotalFac;
        }
        public static class FacturadoAcu
        {
            public static decimal TotalFac;
        }
        public static class Alterviews
        {
            public static string  Alterviewhtml;
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            using (SqlConnection conn2 = new SqlConnection("Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda"))
            {

                Usuarios[] allRecords = null;
                string sql = @"SELECT * FROM dbo.Usuarios_emCCP where PERFIL='GECO' ";
                using (var command = new SqlCommand(sql, conn2))
                {
                    conn2.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        var list = new List<Usuarios>();
                        while (reader.Read())
                            list.Add(new Usuarios { idUSUARIO = reader.GetInt32(0), NOMBRES = reader.GetString(1), APELLIDOS = reader.GetString(2), CORREO = reader.GetString(3), PAIS = reader.GetString(4), PERFIL = reader.GetString(5), COD_VENDEDOR = reader.GetString(6), FECHA_HORACREACION = reader.GetDateTime(7), ACTIVO = reader.GetString(8) });
                        allRecords = list.ToArray();
                    }
                }

                int i = 0;
                while (allRecords.Count() > i)
                {
                    FacturadoAcu.TotalFac = 0;
                    Facturado.TotalFac = 0;
                    GerentesComerciales(allRecords[i].PAIS.ToString(), allRecords[i].CORREO.ToString());
                    ;
                    i++;
                }

            }
            using (SqlConnection conn2 = new SqlConnection("Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda"))
            {

                Usuarios[] allRecords = null;
                string sql = @"SELECT * FROM dbo.Usuarios_emCCP where PERFIL='DIRM' ";
                using (var command = new SqlCommand(sql, conn2))
                {
                    conn2.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        var list = new List<Usuarios>();
                        while (reader.Read())
                            list.Add(new Usuarios { idUSUARIO = reader.GetInt32(0), NOMBRES = reader.GetString(1), APELLIDOS = reader.GetString(2), CORREO = reader.GetString(3), PAIS = reader.GetString(4), PERFIL = reader.GetString(5), COD_VENDEDOR = reader.GetString(6), FECHA_HORACREACION = reader.GetDateTime(7), ACTIVO = reader.GetString(8) });
                        allRecords = list.ToArray();
                    }
                }

                int i = 0;
                while (allRecords.Count() > i)
                {
                    FacturadoAcu.TotalFac = 0;
                    Facturado.TotalFac = 0;
                    GerenteMaquinaria(allRecords[i].CORREO.ToString());
                    ;
                    i++;
                }

            }
            using (SqlConnection conn2 = new SqlConnection("Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda"))
            {

                Usuarios[] allRecords = null;
                string sql = @"SELECT idUSUARIO,NOMBRES,APELLIDOS,CORREO,PAIS,PERFIL,COD_VENDEDOR,FECHA_HORA_CREACION,ACTIVO,REGION =CASE PERFIL WHEN 'DIRN' THEN 'NORTE'WHEN 'DIRS' THEN 'SUR' END FROM dbo.Usuarios_emCCP WHERE PERFIL IN ('DIRN', 'DIRS') ";
                using (var command = new SqlCommand(sql, conn2))
                {
                    conn2.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        var list = new List<Usuarios>();
                        while (reader.Read())
                            list.Add(new Usuarios { idUSUARIO = reader.GetInt32(0), NOMBRES = reader.GetString(1), APELLIDOS = reader.GetString(2), CORREO = reader.GetString(3), PAIS = reader.GetString(4), PERFIL = reader.GetString(5), COD_VENDEDOR = reader.GetString(6), FECHA_HORACREACION = reader.GetDateTime(7), ACTIVO = reader.GetString(8),REGION=reader.GetString(9) });
                        allRecords = list.ToArray();
                    }
                }

                int i = 0;
                
                while (allRecords.Count() > i)
                {
                    FacturadoAcu.TotalFac = 0;
                    Facturado.TotalFac = 0;
                    DirectordeRegion(allRecords[i].REGION.ToString(), allRecords[i].CORREO.ToString());
                    DirectordeRegion(allRecords[i].REGION.ToString(), "carlos.urrutia@emasal.com");
                    ;
                    i++;
                }

            }
            Vendedores();

        }

        public void GerentesComerciales(string PaisGerente,string correo)
        {
            TableMes.TableMesHtml = "";
            TableAcum.TableAcumHtml = "";
            using (SqlConnection conn2 = new SqlConnection("Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda"))
            {


                Metas[] allRecords = null;
                string sql = @"SELECT * FROM dbo.Metas_Venderores_Cumplimiento where pais='"+ PaisGerente + "' and mes='" + DateTime.Now.Month + "' and anho='" + DateTime.Now.Year + "' and PERFIl='ASEM'";
                using (var command = new SqlCommand(sql, conn2))
                {
                    conn2.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        var list = new List<Metas>();
                        while (reader.Read())
                            list.Add(new Metas { Nombres = reader.GetString(0), Apellidos = reader.GetString(1), Correo = reader.GetString(2), Vendeedor = reader.GetString(3), mes = reader.GetInt32(4), Anho = reader.GetInt32(5), Venta = reader.GetDecimal(8), Venta_Meta = reader.GetDecimal(9), Venta_Acumulada = reader.GetDecimal(10), Venta_Meta_Acmulada = reader.GetDecimal(11), Facturas_Realizadas = reader.GetInt32(12), Ventas_Fac_Ant = reader.GetDecimal(13),Pais=reader.GetString(14) });
                        allRecords = list.ToArray();
                    }
                }

                int i = 0;
                Body.Bodyhtml = File.ReadAllText(@"C:\Desarrollos\MetasVendedores\MetaVendedores\MetaVendedores\HTMLPage1.html");
                Body.Bodyhtml = Body.Bodyhtml + "<p align='left' class='a' style=' font-family: sans-serif;'> <br> Los resultados del mes son:</p><br> <br>";
                Body.Bodyhtml = Body.Bodyhtml + "<table class='Bordered' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;border-color:#ddd;'><tr><th>Vendedor</th><th>Periodo</th><th>Facturado a la fecha</th><th>Meta de Mes</th><th>Cumplimiento</th><th><font color='red'>Déficit/</font><font color='grenn'>Exceso</font></th><th>KPI</th></tr></tr>";
                
                while (allRecords.Count() > i)
                {

                    ArmarHTMLGerente(allRecords[i]);
                    i++;
                }
                ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                string separador = "</table>   <br><br><br>  Total Facturado de Mes:  $" + string.Format("{0:0,0}",Facturado.TotalFac) + "  <br><br><br> <table class='Bordered' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;border-color:#ddd;'><tr><th>Pais</th><th>Vendedor</th><th>Periodo</th><th>Facturado a la fecha</th><th>Meta de Año</th><th>Cumplimiento</th><th><font color='red'>Déficit/</font><font color='grenn'>Exceso</font></th><th>KPI</th></tr></tr>";
                Body.Bodyhtml = Body.Bodyhtml + TableMes.TableMesHtml + separador + TableAcum.TableAcumHtml + "</table><br><br><br> Total Facturado Acumulado:  $" + string.Format("{0:0,0}", FacturadoAcu.TotalFac) ; 
                string body = Body.Bodyhtml.ToString();
                
                var fromAddress = new MailAddress("notificaciones@emasal.com", "From Name");
           
                var toAddress = new MailAddress(correo, "To Name");
                const string fromPassword = "ema$al777";
                const string subject = "Tus Resultados de Ventas - Grupo Emasal";
             
                var smtp = new SmtpClient
                {
                    Host = "smtp.office365.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                   // AlternateViews = { Alterviews.Alterviewhtml }
                })
                {


                    smtp.Send(message);
                }
            }
        }
        public void GerenteMaquinaria( string correo)
        {
            TableMes.TableMesHtml = "";
            TableAcum.TableAcumHtml = "";
            using (SqlConnection conn2 = new SqlConnection("Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda"))
            {


                Metas[] allRecords = null;
                string sql = @"SELECT * FROM dbo.Metas_Venderores_Cumplimiento where mes='" + DateTime.Now.Month + "' and anho='" + DateTime.Now.Year + "' and PERFIl='ASMA'";
                using (var command = new SqlCommand(sql, conn2))
                {
                    conn2.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        var list = new List<Metas>();
                        while (reader.Read())
                            list.Add(new Metas { Nombres = reader.GetString(0), Apellidos = reader.GetString(1), Correo = reader.GetString(2), Vendeedor = reader.GetString(3), mes = reader.GetInt32(4), Anho = reader.GetInt32(5), Venta = reader.GetDecimal(8), Venta_Meta = reader.GetDecimal(9), Venta_Acumulada = reader.GetDecimal(10), Venta_Meta_Acmulada = reader.GetDecimal(11), Facturas_Realizadas = reader.GetInt32(12), Ventas_Fac_Ant = reader.GetDecimal(13), Pais = reader.GetString(14) });
                        allRecords = list.ToArray();
                    }
                }

                int i = 0;
                Body.Bodyhtml = File.ReadAllText(@"C:\Desarrollos\MetasVendedores\MetaVendedores\MetaVendedores\HTMLPage1.html");
                Body.Bodyhtml = Body.Bodyhtml + "<p align='left' class='a' style=' font-family: sans-serif;'> <br> Los resultados del mes son:</p><br> <br>";
                Body.Bodyhtml = Body.Bodyhtml + "<table class='Bordered' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;border-color:#ddd;'><tr><th>Pais</th><th>Vendedor</th><th>Periodo</th><th>Facturado a la fecha</th><th>Meta de Mes</th><th>Cumplimiento</th><th><font color='red'>Déficit/</font><font color='grenn'>Exceso</font></th><th>KPI</th></tr></tr>";

                while (allRecords.Count() > i)
                {

                    ArmarHTMLGerente(allRecords[i]);
                    i++;
                }

                ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                string separador = "</table>   <br><br><br>  Total Facturado:  $" + string.Format("{0:0,0}", Facturado.TotalFac) + "  <br><br><br> <table class='Bordered' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;border-color:#ddd;'><tr><th>Pais</th><th>Vendedor</th><th>Periodo</th><th>Facturado a la fecha</th><th>Meta de Año</th><th>Cumplimiento</th><th><font color='red'>Déficit/</font><font color='grenn'>Exceso</font></th><th>KPI</th></tr></tr>";
                Body.Bodyhtml = Body.Bodyhtml + TableMes.TableMesHtml + separador + TableAcum.TableAcumHtml + "<br><br><br> Total Facturado Acumulado:  $" + string.Format("{0:0,0}", FacturadoAcu.TotalFac);
                string body = Body.Bodyhtml.ToString();

                var fromAddress = new MailAddress("notificaciones@emasal.com", "From Name");

                var toAddress = new MailAddress(correo, "To Name");
                const string fromPassword = "ema$al777";
                const string subject = "Tus Resultados de Ventas - Grupo Emasal";

                var smtp = new SmtpClient
                {
                    Host = "smtp.office365.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                    // AlternateViews = { Alterviews.Alterviewhtml }
                })
                {


                    smtp.Send(message);
                }
            }
        }
        public void DirectordeRegion(string region, string correo)
        {
            TableMes.TableMesHtml = "";
            TableAcum.TableAcumHtml = "";
            using (SqlConnection conn2 = new SqlConnection("Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda"))
            {


                Metas[] allRecords = null;
                string sql = @"SELECT * FROM dbo.Metas_Venderores_Cumplimiento where mes='" + DateTime.Now.Month + "' and anho='" + DateTime.Now.Year + "' and REGION='"+region+"'";
                using (var command = new SqlCommand(sql, conn2))
                {
                    conn2.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        var list = new List<Metas>();
                        while (reader.Read())
                            list.Add(new Metas { Nombres = reader.GetString(0), Apellidos = reader.GetString(1), Correo = reader.GetString(2), Vendeedor = reader.GetString(3), mes = reader.GetInt32(4), Anho = reader.GetInt32(5), Venta = reader.GetDecimal(8), Venta_Meta = reader.GetDecimal(9), Venta_Acumulada = reader.GetDecimal(10), Venta_Meta_Acmulada = reader.GetDecimal(11), Facturas_Realizadas = reader.GetInt32(12), Ventas_Fac_Ant = reader.GetDecimal(13), Pais = reader.GetString(14) });
                        allRecords = list.ToArray();
                    }
                }

                int i = 0;
                Body.Bodyhtml = File.ReadAllText(@"C:\Desarrollos\MetasVendedores\MetaVendedores\MetaVendedores\HTMLPage1.html");
                Body.Bodyhtml = Body.Bodyhtml + "<p align='left' class='a' style=' font-family: sans-serif;'> <br> Los resultados del mes son:</p><br> <br>";
                Body.Bodyhtml = Body.Bodyhtml + "<table class='Bordered' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;border-color:#ddd;'><tr><th>Pais</th><th>Vendedor</th><th>Periodo</th><th>Facturado a la fecha</th><th>Meta de Mes</th><th>Cumplimiento</th><th><font color='red'>Déficit/</font><font color='grenn'>Exceso</font></th><th>KPI</th></tr></tr>";

                while (allRecords.Count() > i)
                {

                    ArmarHTMLGerente(allRecords[i]);
                    i++;
                }
                ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                string separador = "</table>   <br><br><br>  Total Facturado:  $" + string.Format("{0:0,0}", Facturado.TotalFac) + "  <br><br><br> <table class='Bordered' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;border-color:#ddd;'><tr><th>Pais</th><th>Vendedor</th><th>Periodo</th><th>Facturado a la fecha</th><th>Meta de Año</th><th>Cumplimiento</th><th><font color='red'>Déficit/</font><font color='grenn'>Exceso</font></th><th>KPI</th></tr></tr>";
                Body.Bodyhtml = Body.Bodyhtml + TableMes.TableMesHtml + separador + TableAcum.TableAcumHtml + "</table><br><br><br> Total Facturado Acumulado:  $" + string.Format("{0:0,0}", FacturadoAcu.TotalFac);
                string body = Body.Bodyhtml.ToString();

                var fromAddress = new MailAddress("notificaciones@emasal.com", "From Name");

                var toAddress = new MailAddress(correo, "To Name");
                const string fromPassword = "ema$al777";
                string subject = "";
                if (region == "NORTE")
                {
                     subject = " Resultados de Region Norte - Grupo Emasal";
                    
                }
                else
                {
                    subject = " Resultados de Region Sur - Grupo Emasal";

                }
                 



                var smtp = new SmtpClient
                {
                    Host = "smtp.office365.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                    // AlternateViews = { Alterviews.Alterviewhtml }
                })
                {


                    smtp.Send(message);
                }
            }
        }

        public void Vendedores ()
            {
            using (SqlConnection conn2 = new SqlConnection("Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda"))
            {


                Metas[] allRecords = null;
                string sql = @"SELECT * FROM dbo.Metas_Venderores_Cumplimiento where mes='" + DateTime.Now.Month + "' and anho='" + DateTime.Now.Year + "'";
                using (var command = new SqlCommand(sql, conn2))
                {
                    conn2.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        var list = new List<Metas>();
                        while (reader.Read())
                            list.Add(new Metas { Nombres = reader.GetString(0), Apellidos = reader.GetString(1), Correo = reader.GetString(2), Vendeedor = reader.GetString(3), mes = reader.GetInt32(4), Anho = reader.GetInt32(5), Venta = reader.GetDecimal(8), Venta_Meta = reader.GetDecimal(9), Venta_Acumulada = reader.GetDecimal(10), Venta_Meta_Acmulada = reader.GetDecimal(11), Facturas_Realizadas = reader.GetInt32(12), Ventas_Fac_Ant = reader.GetDecimal(13),Pais=reader.GetString(14) });
                        allRecords = list.ToArray();
                    }
                }

                int i = 0;
                while (allRecords.Count() > i)
                {

                    ArmarHTML(allRecords[i]);

                    i++;
                }

            }
        }
        public void ArmarHTML(Metas meta)
        {
            int porcentaje;
            int porcentaje2;
            try
            {

                 porcentaje = Convert.ToInt32(((meta.Venta / meta.Venta_Meta) * 100));
            }
            catch (DivideByZeroException)
            {

                porcentaje=0;
                
            }
            try
            {

                 porcentaje2 = Convert.ToInt32(((meta.Venta_Acumulada / meta.Venta_Meta_Acmulada) * 100));
            }
            catch (DivideByZeroException)
            {

                porcentaje2 = 0; ;
            }
          
            string img;
            string img2="";
            if (porcentaje > 99)
            {
                img = @"C:\Desarrollos\MetasVendedores\Rcs\Sverde.png";
            }
            else
            {
                if (porcentaje > 91)
                {
                    img = @"C:\Desarrollos\MetasVendedores\Rcs\SAmarillo.png";
                }
                else {
                    img = @"C:\Desarrollos\MetasVendedores\Rcs\SRojo.png";
                }
            };
            if (porcentaje2 > 99)
            {
                img2 = @"C:\Desarrollos\MetasVendedores\Rcs\Sverde.png";
            }
            else
            {
                if (porcentaje2 > 91)
                {
                    img2 = @"C:\Desarrollos\MetasVendedores\Rcs\SAmarillo.png";
                }
                else
                {
                    img2 = @"C:\Desarrollos\MetasVendedores\Rcs\SRojo.png";
                }
            };
            decimal var;
            decimal var2;
            var= Decimal.Round((meta.Venta - meta.Venta_Meta), 2);
            var2= Decimal.Round((meta.Venta_Acumulada - meta.Venta_Meta_Acmulada), 2) ;
            string color;
            string color2;
            if (var<0)
            {
                color = "red";
            }
            else
            {
                color = "green";
            }
            if (var2 < 0)
            {
                color2 = "red";
            }
            else
            {
                color2 = "green";
            }

            var linkedResource = new LinkedResource(img.ToString(), MediaTypeNames.Image.Jpeg);
            var linkedResource2 = new LinkedResource(img2.ToString(), MediaTypeNames.Image.Jpeg);
            Html.HtmlString= File.ReadAllText(@"C:\Desarrollos\MetasVendedores\MetaVendedores\MetaVendedores\HTMLPage1.html");
            Html.HtmlString = Html.HtmlString.ToString() + "<p align = 'left' class='a' style=' font-family: sans-serif;'> Hola, <strong> " + meta.Nombres.ToString() +" "+meta.Apellidos.ToString()+"</strong><br> <br> Ayer generastes <strong> "+meta.Facturas_Realizadas+" </strong> Facturas con un total de<strong> $"+ string.Format("{0:0,0}",meta.Ventas_Fac_Ant)+"</strong> <p>";
            Html.HtmlString = Html.HtmlString.ToString() + "<p align='left' class='a' style=' font-family: sans-serif;'> <br>Tus resultados del mes son:</p><br> <br>";
            Html.HtmlString = Html.HtmlString.ToString() + "<table class='Bordered' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;border-color:#ddd;'><tr><th>Periodo</th><th>Facturado a la fecha</th><th>Meta de Mes</th><th>Cumplimiento</th><th><font color='red'>Déficit/</font><font color='grenn'>Exceso</font></th><th>KPI</th></tr></tr><tr><td> " + meta.mes + " /" + meta.Anho + "</td><td> $" + string.Format("{0:0,0}", Decimal.Round(meta.Venta,2)) + "</td><td> $" + string.Format("{0:0,0}", Decimal.Round(meta.Venta_Meta,2)) + "</td><td>" + porcentaje + "%</td> <td><font color='"+color+"'>$" + string.Format("{0:0,0}", var) + "</font></td><td>";
            Html.HtmlString = Html.HtmlString.ToString() + $"<img src=\"cid:{linkedResource.ContentId}\"/></td></tr></table><br> <br><br> <p class='a' align='leftr' style=' font-family: sans-serif; '>  Tu Resultado del año es:</strong></p>";

            //2table
            Html.HtmlString = Html.HtmlString.ToString() + "<table class='Bordered' border='1' cellpadding='0' cellspacing='1' bordercolor='#000000' style='border-collapse:collapse;border-color:#ddd;'><tr><th>Periodo</th><th>Facturado a la fecha</th><th>Meta de Año</th><th>Cumplimiento</th><th><font color='red'>Déficit/</font><font color='grenn'>Exceso</font></th><th>KPI</th></tr></tr><tr><td> " + meta.Anho + "</td><td> $" + string.Format("{0:0,0}", Decimal.Round(meta.Venta_Acumulada,2)) + "</td><td>$" + string.Format("{0:0,0}", Decimal.Round(meta.Venta_Meta_Acmulada,2)) + "</td><td>" + porcentaje2 + "%</td> <td><font color='"+color2+"'>$" + string.Format("{0:0,0}", var2) + "</font></td><td>";
            Html.HtmlString = Html.HtmlString.ToString() + $"<img src=\"cid:{linkedResource2.ContentId}\"/></td></tr></table><br> <br><br> <p class='a' align='leftr' style=' font-family: sans-serif; '>  </strong></p><br> <Con esfuerzo y dedicación estamos seguros que lograras tus objetivos , Adelante ¡!<br>Grupo Emasal </html>";


            var alternateView = AlternateView.CreateAlternateViewFromString(Html.HtmlString.ToString(), null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(linkedResource);
            alternateView.LinkedResources.Add(linkedResource2);
            string htm = Html.HtmlString.ToString();
            ContentType mimeType = new System.Net.Mime.ContentType("text/html");

            string body = htm;
            //AlternateView alternate = AlternateView.CreateAlternateViewFromString(body, mimeType);
            var fromAddress = new MailAddress("notificaciones@emasal.com", "From Name");
            //Aqui un if compañaia
            var toAddress = new MailAddress(meta.Correo.ToString(), "To Name");
            const string fromPassword = "ema$al777";
            const string subject = "Tus Resultados de Ventas - Grupo Emasal";
            // My mail provider would not accept an email with only an image, adding hello so that the content looks less suspicious.
            var smtp = new SmtpClient
            {
                Host = "smtp.office365.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                IsBodyHtml = true,
                Subject = subject,
                Body = body,
                AlternateViews = { alternateView }
            })
            {


                smtp.Send(message);
            }
            Html.HtmlString = "";
        }
        public void ArmarHTMLGerente(Metas meta)
        {
            int porcentaje;
            int porcentaje2;
            try
            {

                porcentaje = Convert.ToInt32(((meta.Venta / meta.Venta_Meta) * 100));
            }
            catch (DivideByZeroException)
            {

                porcentaje = 0;

            }
            try
            {

                porcentaje2 = Convert.ToInt32(((meta.Venta_Acumulada / meta.Venta_Meta_Acmulada) * 100));
            }
            catch (DivideByZeroException)
            {

                porcentaje2 = 0; ;
            }

            string img;
            string img2 = "";
            if (porcentaje > 99)
            {
                img = @"C:\Desarrollos\MetasVendedores\Rcs\Sverde.png";
            }
            else
            {
                if (porcentaje > 91)
                {
                    img = @"C:\Desarrollos\MetasVendedores\Rcs\SAmarillo.png";
                }
                else
                {
                    img = @"C:\Desarrollos\MetasVendedores\Rcs\SRojo.png";
                }
            };
            if (porcentaje2 > 99)
            {
                img2 = @"C:\Desarrollos\MetasVendedores\Rcs\Sverde.png";
            }
            else
            {
                if (porcentaje2 > 91)
                {
                    img2 = @"C:\Desarrollos\MetasVendedores\Rcs\SAmarillo.png";
                }
                else
                {
                    img2 = @"C:\Desarrollos\MetasVendedores\Rcs\SRojo.png";
                }
            };
            decimal var;
            decimal var2;
            var = Decimal.Round((meta.Venta - meta.Venta_Meta), 2);
            var2 = Decimal.Round((meta.Venta_Acumulada - meta.Venta_Meta_Acmulada), 2);
            string color;
            string color2;
            if (var < 0)
            {
                color = "red";
            }
            else
            {
                color = "green";
            }
            if (var2 < 0)
            {
                color2 = "red";
            }
            else
            {
                color2 = "green";
            }

            var linkedResource = new LinkedResource(img.ToString(), MediaTypeNames.Image.Jpeg);
            var linkedResource2 = new LinkedResource(img2.ToString(), MediaTypeNames.Image.Jpeg);
            TableMes.TableMesHtml= TableMes.TableMesHtml.ToString() + "<tr><td> " + meta.Pais +"</td><td> " + meta.Nombres + " " + meta.Apellidos + "</td><td> " + meta.mes + "/" + meta.Anho + "</td><td> $" + string.Format("{0:0,0}", Decimal.Round(meta.Venta, 2)) + "</td><td> $" + string.Format("{0:0,0}", Decimal.Round(meta.Venta_Meta, 2)) + "</td><td>" + porcentaje + "%</td> <td><font color='" + color + "'>$" + string.Format("{0:0,0}", var) + "</font></td><td>";
            TableMes.TableMesHtml = TableMes.TableMesHtml.ToString() + $"<font size='8' color='"+color+ "'>O</font></td></tr>";
            Facturado.TotalFac = Facturado.TotalFac + meta.Venta;
            //2table
            TableAcum.TableAcumHtml = TableAcum.TableAcumHtml.ToString() + "<tr><td> " + meta.Pais + "</td><td> " + meta.Nombres +" "+meta.Apellidos+ "</td><td> " + meta.Anho + "</td><td> $" + string.Format("{0:0,0}", Decimal.Round(meta.Venta_Acumulada, 2)) + "</td><td>$" + string.Format("{0:0,0}", Decimal.Round(meta.Venta_Meta_Acmulada, 2)) + "</td><td>" + porcentaje2 + "%</td> <td><font color='" + color2 + "'>$" + string.Format("{0:0,0}", var2) + "</font></td><td>";
            TableAcum.TableAcumHtml = TableAcum.TableAcumHtml.ToString() + $"<font size='8' color='" + color2 + "'>O</font></td></tr>";
            FacturadoAcu.TotalFac = FacturadoAcu.TotalFac+meta.Venta_Acumulada;

            //  Alterviews.Alterviewhtml.LinkedResources.Add(linkedResource);
            //Alterviews.Alterviewhtml.LinkedResources.Add(linkedResource2);

        }

    }
}
