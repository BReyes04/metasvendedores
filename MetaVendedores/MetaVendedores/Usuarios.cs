﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaVendedores
{
    class Usuarios
    {
        public int idUSUARIO { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
        public string CORREO { get; set; }
        public string PAIS { get; set; }
        public string PERFIL { get; set; }
        public string COD_VENDEDOR { get; set; }

        public DateTime FECHA_HORACREACION { get; set; }

        public string ACTIVO { get; set; }
        public string REGION { get; set; }
      
    }
}
