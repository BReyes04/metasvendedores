﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaVendedores
{
    public class Metas
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public string Vendeedor { get; set; }
        public int mes { get; set; }
        public int Anho { get; set; }
        public decimal Venta { get; set; }
        public decimal Venta_Meta { get; set; }
        public decimal Venta_Acumulada { get; set; }
        public decimal Venta_Meta_Acmulada { get; set; }

        public int Facturas_Realizadas { get; set; }
        public decimal Ventas_Fac_Ant { get; set; }
        public string Pais { get; set; }
        public string Region { get; set; }

    }
}
